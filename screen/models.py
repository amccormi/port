# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.core.validators import FileExtensionValidator


class Post(models.Model):
    title = models.CharField(max_length=200)
    headerPhoto = models.ImageField(upload_to='images/', blank=True, null=True, validators=[FileExtensionValidator(['jpg'])])
    body = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title