# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-12-31 01:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('screen', '0009_post_headerphoto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='headerPhoto',
            field=models.ImageField(blank=True, null=True, upload_to='images/'),
        ),
    ]
