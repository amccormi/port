# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-09 18:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('screen', '0005_auto_20180209_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='closed',
            field=models.CharField(blank=True, default='The Dinosaurs are sleeping', max_length=200),
        ),
    ]
